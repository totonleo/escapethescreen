#include "Tilemap.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <cctype>
#include <string>

#define TILESIZE 40

using namespace sf;

void Tilemap::LoadLevel(const char*level)
{

    std::ifstream openfile(level);
    counter = Vector2i(0,0); //Reset � chaque d�but
    if(openfile.is_open())
    {
        std::string tilelocation;
        openfile >> tilelocation; // Lecture premi�re ligne du fichier pour le tile map texture
        tileTexture.loadFromFile(tilelocation); //Load la texture depuis le fichier texte
        tile.setTexture(tileTexture); //applique la texture a la tile
        while(!openfile.eof()) //Tant que on n'atteint pas la fin du fichier : EndOfFile
        {
            std::string str;
            openfile >> str; //Lecture du couple x,x
            char x = str[0]; //Premi�re variable avant la virgule
            char y = str[2]; //Deuxieme variable
            if (!isdigit(x) || !isdigit(y)) //Check si x et y sont des nombres
                tilemap[counter.x][counter.y] = Vector2i(-1,-1); // Si �a ne l'est pas alors -1 au x et y du compteur ...
            else
                tilemap[counter.x][counter.y] = Vector2i(x-'0', y-'0'); // ... sinon transforme le texte n chiffre
            if(openfile.peek()=='\n') //Dans le cas d'un retour � la ligne
            {
                counter.x=0;   //Set du counter en x � zero et ajout d'un y vu qu'on augmente d'une ligne
                counter.y++;
            }
            else
                counter.x++;
        }
        counter.y++;//
    }

}
void Tilemap::DrawLevel(RenderWindow *win)
{
    (*win).clear();
for (int i=0; i<counter.x; i++) //traite chaque couple de variable et augmente de 1 � chaque fois
        {

            for (int j=0; j<counter.y; j++) // traite la ligne
            {

                if(tilemap[i][j].x !=-1 && tilemap[i][j].y != -1)
                {

                    tile.setPosition(i*TILESIZE, j*TILESIZE); // Multiple pour avoir la bonne size des textures
                    tile.setTextureRect(sf::IntRect(tilemap[i][j].x *TILESIZE,tilemap[i][j].y * TILESIZE,TILESIZE,TILESIZE));
                    (*win).draw(tile);
                }
            }
        }
}
void Tilemap::LoadColisionLevel(const char*levelcol) //Gros boulot dessus
{
    int cpt1=0,cpt0=0;
    std::ifstream openfile(levelcol);
    countercol = Vector2i(0,0);
    if(openfile.is_open())
    {
        while(!openfile.eof()) //Tant que on n'atteint pas la fin du fichier : EndOfFile
        {
            std::string str;
            openfile >> str;
            char x = str[0]; //Premi�re variable avant l'espace
            if (x=='1') //Check si x est egal au 0
            {
                colisionmap[countercol.x][countercol.y] = 1;
                cpt1++; // Si �a ne l'est pas alors 0 au x et y du compteur
            }
            else if (x=='0')
            {
                colisionmap[countercol.x][countercol.y] = 0;
                cpt0++;
            }
                  // Sinon transforme le texte n chiffre
            if(openfile.peek()=='\n') //Dans le cas d'un retour � la ligne
            {
                countercol.x=0;  //Set du counter en x � zero et ajout d'un y vu qu'on augmente d'une ligne

                countercol.y++;

            }
            else
                countercol.x++;

        }
        countercol.y++;

    }
    printf("nbr1 : %i et nbr0 : %i", cpt1, cpt0);
}





void Tilemap::DrawColision(Player *p)
{
 (*p).Update();
 (*p).rect.setFillColor(Color::Blue); //When player dont colide

for (int i=0; i<countercol.x; i++) //traite chaque couple de variable et augmente de 1 � chaque fois
        {

            for (int j=0; j<countercol.y; j++) // traite la ligne
            {

                if(colisionmap[i][j]==1)
                {
                    int bottom, top, left, right;
                    bottom=j*TILESIZE+TILESIZE;
                    top=j*TILESIZE;
                    left=i*TILESIZE;
                    right=i*TILESIZE+TILESIZE;



                    if(!((*p).droite < left || (*p).gauche > right || (*p).haut > bottom || (*p).bas < top))
                    {
                        (*p).rect.setFillColor(Color::Red);
                        (*p).sol=top;
                        printf("Colide\n");
                    }
                    else
                    {

                    }

                }
            }
        }
}
