#ifndef PLAYER_H
#define PLAYER_H
#include <SFML/Graphics.hpp>
using namespace sf;

class Player
{
    public:
        Player(Vector2f position, Vector2f taille);

        int haut, gauche, droite, bas, sol;
        RectangleShape rect;
        Vector2f velocity;
        void Update();
};

#endif // PLAYER_H
