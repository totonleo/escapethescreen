#ifndef TILEMAP_H
#define TILEMAP_H
#include <SFML/Graphics.hpp>
#include "Player.h"
using namespace sf;

class Tilemap
{
    public:
    Texture tileTexture;
    Sprite tile;
    Vector2i tilemap[48][27];
    int colisionmap[48][27];
    Vector2i counter;
    Vector2i countercol;
    void LoadLevel(const char*level);
    void LoadColisionLevel(const char*levelcol);
    void DrawColision(Player *p);
    void DrawLevel(RenderWindow *win);
};

#endif // TILEMAP_H
