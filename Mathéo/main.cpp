#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <cctype>
#include <string>
#include "Player.h"
#include "Tilemap.h"



using namespace sf;
/*
class Player
{
public:

    RectangleShape rect;
    Vector2f velocity=Vector2f(0,0);
    int sol=600;

    int haut, gauche, droite, bas;

    Player(Vector2f position, Vector2f taille)
    {
        rect.setPosition(position);
        rect.setSize(taille);
    }

    void Update() //Si changment de l'origine sinon refaire (centre +- halfsize de chaque cot�)
    {
        bas = rect.getPosition().y + rect.getSize().y;
        gauche = rect.getPosition().x;
        droite = rect.getPosition().x + rect.getSize().x;
        haut = rect.getPosition().y;
    }

};
*/
/*
class Tilemap
{
public :
    Texture tileTexture;
    Sprite tile;
    Vector2i tilemap[48][27];
    int colisionmap[48][27];
    Vector2i counter = Vector2i(0,0);
    Vector2i countercol = Vector2i(0,0);


void LoadLevel(const char*level)
{
    std::ifstream openfile(level);
    counter = Vector2i(0,0); //Reset � chaque d�but
    if(openfile.is_open())
    {
        std::string tilelocation;
        openfile >> tilelocation; // Lecture premi�re ligne du fichier pour le tile map texture
        tileTexture.loadFromFile(tilelocation); //Load la texture depuis le fichier texte
        tile.setTexture(tileTexture); //applique la texture a la tile
        while(!openfile.eof()) //Tant que on n'atteint pas la fin du fichier : EndOfFile
        {
            std::string str;
            openfile >> str; //Lecture du couple x,x
            char x = str[0]; //Premi�re variable avant la virgule
            char y = str[2]; //Deuxieme variable
            if (!isdigit(x) || !isdigit(y)) //Check si x et y sont des nombres
                tilemap[counter.x][counter.y] = Vector2i(-1,-1); // Si �a ne l'est pas alors -1 au x et y du compteur ...
            else
                tilemap[counter.x][counter.y] = Vector2i(x-'0', y-'0'); // ... sinon transforme le texte n chiffre
            if(openfile.peek()=='\n') //Dans le cas d'un retour � la ligne
            {
                counter.x=0;   //Set du counter en x � zero et ajout d'un y vu qu'on augmente d'une ligne
                counter.y++;
            }
            else
                counter.x++;
        }
        counter.y++;//
    }

}
void LoadColisionLevel(const char*levelcol) //Gros boulot dessus
{
    int cpt1=0,cpt0=0;
    std::ifstream openfile(levelcol);
    countercol = Vector2i(0,0);
    if(openfile.is_open())
    {
        while(!openfile.eof()) //Tant que on n'atteint pas la fin du fichier : EndOfFile
        {
            std::string str;
            openfile >> str;
            char x = str[0]; //Premi�re variable avant l'espace
            if (x=='1') //Check si x est egal au 0
            {
                colisionmap[countercol.x][countercol.y] = 1;
                cpt1++; // Si �a ne l'est pas alors 0 au x et y du compteur
            }
            else if (x=='0')
            {
                colisionmap[countercol.x][countercol.y] = 0;
                cpt0++;
            }
                  // Sinon transforme le texte n chiffre
            if(openfile.peek()=='\n') //Dans le cas d'un retour � la ligne
            {
                countercol.x=0;  //Set du counter en x � zero et ajout d'un y vu qu'on augmente d'une ligne

                countercol.y++;

            }
            else
                countercol.x++;

        }
        countercol.y++;

    }
    printf("nbr1 : %i et nbr0 : %i", cpt1, cpt0);
}
void DrawColision(Player *p)
{
 (*p).Update();
 (*p).rect.setFillColor(Color::Blue); //When player dont colide

for (int i=0; i<countercol.x; i++) //traite chaque couple de variable et augmente de 1 � chaque fois
        {

            for (int j=0; j<countercol.y; j++) // traite la ligne
            {

                if(colisionmap[i][j]==1)
                {
                    int bottom, top, left, right;
                    bottom=j*TILESIZE+TILESIZE;
                    top=j*TILESIZE;
                    left=i*TILESIZE;
                    right=i*TILESIZE+TILESIZE;



                    if(!((*p).droite < left || (*p).gauche > right || (*p).haut > bottom || (*p).bas < top))
                    {
                        (*p).rect.setFillColor(Color::Red);
                        (*p).sol=top;
                        printf("Colide\n");
                    }
                    else
                    {

                    }

                }
            }
        }
}

void DrawLevel(RenderWindow *win){
    (*win).clear();
for (int i=0; i<counter.x; i++) //traite chaque couple de variable et augmente de 1 � chaque fois
        {

            for (int j=0; j<counter.y; j++) // traite la ligne
            {

                if(tilemap[i][j].x !=-1 && tilemap[i][j].y != -1)
                {

                    tile.setPosition(i*TILESIZE, j*TILESIZE); // Multiple pour avoir la bonne size des textures
                    tile.setTextureRect(sf::IntRect(tilemap[i][j].x *TILESIZE,tilemap[i][j].y * TILESIZE,TILESIZE,TILESIZE));
                    (*win).draw(tile);
                }
            }
        }
}

};
*/




int main()
{

    RenderWindow windows(VideoMode(800,600), "Tile maps");
    windows.setFramerateLimit(60); //Obvious sinon descente trop rapide

  //  int sol = 400; // To do : R�cup�rer "hauteur" de colision � l'instantan� et l'appliquer � sol. Update constant
    const float gravity = 1.0f;
    float horizontalspeed=4.0f;
    float verticalspeed=10.0f;




    Tilemap lvl1;
    Player p1(Vector2f(400,50), Vector2f(40,40));
    p1.rect.setFillColor(Color::Blue);

    while(windows.isOpen())
    {
        Event event;
        while(windows.pollEvent(event))
        {
            switch (event.type)
            {
            case Event::Closed:
                windows.close();
                break;
            case Event::KeyPressed:
                if(event.key.code == Keyboard::F)
                {
                    lvl1.LoadLevel("level1.txt");
                    lvl1.LoadColisionLevel("colision1.txt");
                }
                break;
            }
        }

        if(Keyboard::isKeyPressed(Keyboard::Right))
        {
            p1.velocity.x=horizontalspeed;
        }
        else if (Keyboard::isKeyPressed(Keyboard::Left))
        {
            p1.velocity.x= -horizontalspeed;
        }
        else
        {
            p1.velocity.x = 0;
        }

        if(Keyboard::isKeyPressed(Keyboard::Up))
        {
                p1.velocity.y=-verticalspeed; //Impl�menter un max airtime
        }
        if (p1.rect.getPosition().y + p1.rect.getSize().y< p1.sol || p1.velocity.y<0)
        {
            p1.velocity.y += gravity;
        }
        else
        {
            p1.rect.setPosition(p1.rect.getPosition().x, p1.sol-p1.rect.getSize().y);
            p1.velocity.y=0;
        }

        p1.rect.move(p1.velocity.x,p1.velocity.y);

       /* if(Keyboard::isKeyPressed(Keyboard::Right))
        {
            p1.velocity.x=horizontalspeed;
        }
        else if (Keyboard::isKeyPressed(Keyboard::Left))
        {
            p1.velocity.x= -horizontalspeed;
        }
        else
        {
            p1.velocity.x = 0;
        }

        if(Keyboard::isKeyPressed(Keyboard::Up))
        {
            p1.velocity.y=-verticalspeed; //Impl�menter un max airtime
        }
        else if(Keyboard::isKeyPressed(Keyboard::Down))
        {
                p1.velocity.y=+verticalspeed;
        }
        else
        {
            p1.velocity.y=0;
        }
        if (p1.rect.getPosition().y + p1.rect.getSize().y< p1.sol || p1.velocity.y<0)
        {
            p1.velocity.y += gravity;
        }
        else
        {
            p1.rect.setPosition(p1.rect.getPosition().x, p1.sol-p1.rect.getSize().y);
            p1.velocity.y=0;
        }

        */



        //Movement to do : Import Ralentissement g�n�ral depuis my game.

        windows.clear();

        lvl1.DrawLevel(&windows);
        lvl1.DrawColision(&p1);
        windows.draw(p1.rect);
        p1.Update();



        windows.display();

    }
}
