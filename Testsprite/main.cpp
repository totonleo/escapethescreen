#include <SFML/Graphics.hpp>
#define bout 20

using namespace sf;

typedef struct
{
    int x;
    int y;
} Pos;

void checkbout(RectangleShape souris, CircleShape bouton);

int main()
{
    Event evenement;

    Pos pospers = {0,400};
    Pos posbout1 = {500,380};

    RenderWindow fenetre(VideoMode(1200, 600), "test sprites");

    RectangleShape rect(Vector2f(1200, 10));
    rect.setFillColor(Color:: White);
    rect.setPosition(0,500);

    CircleShape bout1(10);
    bout1.setFillColor(Color::Red);
    bout1.setPosition(Vector2f(posbout1.x,posbout1.y));

    RectangleShape souris(Vector2f(6,6));
    souris.setOutlineColor(Color::White);
    souris.setOutlineThickness(-1);

    Texture stand;                stand.loadFromFile("sprite/0.png");
    Texture move1;                move1.loadFromFile("sprite/1.png");
    Texture move2;                move2.loadFromFile("sprite/2.png");
    Texture move3;                move3.loadFromFile("sprite/3.png");
    Sprite sprite(stand);
    sprite.setPosition(Vector2f(pospers.x,pospers.y));

    int i = 0, j = 0;


    while (1)
    {
        while(fenetre.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::Closed:
            {
                fenetre.close();
                break;
            }
            case Event::MouseButtonPressed:
                if (evenement.mouseButton.button == Mouse::Left)
                {
                    checkbout(souris,bout1);
                    printf("%i  %i\n",evenement.mouseButton.x,evenement.mouseButton.y);
                }
                break;
            case Event::MouseMoved:
                souris.setPosition(Vector2f(evenement.mouseMove.x, evenement.mouseMove.y));
            case Event::KeyPressed:
                if (evenement.key.code == Keyboard::Right)
                {
                    pospers.x += 3;
                    if (i<=5)
                        sprite.setTexture(move1);
                    if (i>10 && i<=15)
                        sprite.setTexture(move2);
                    if (i>15)
                        i=0;
                    i++;
                }
                if (evenement.key.code == Keyboard::Left)
                {
                    pospers.x -= 2;
                    if (j<=5)
                        sprite.setTexture(move2);
                    if (j>10 && j<=15)
                        sprite.setTexture(move3);
                    if (j>15)
                        j=0;
                    j++;
                }
                break;
            }
        }
        if (!(Keyboard::isKeyPressed(Keyboard::Left) || Keyboard::isKeyPressed(Keyboard::Right)))
        {
            sprite.setTexture(stand);
            i=0, j=0;
        }
        fenetre.clear();
        sprite.setPosition(Vector2f(pospers.x,pospers.y));
        fenetre.draw(rect);
        fenetre.draw(bout1);
        fenetre.draw(sprite);
        fenetre.display();
    }
    return EXIT_SUCCESS;
}

void checkbout(RectangleShape souris, CircleShape bouton)
{
    if (bouton.getGlobalBounds().intersects(souris.getGlobalBounds()))
        printf("ok\n");
}






/*
void creerBouton();
void creerBouton()
{

}
*/





















