#Escape The Screen
##### Credit :
-Léo Toton
-Robin Bochu
-Mathéo Auer
-Lola Tournayre

------------



#### Reste à faire : 
###### Logique
- Fonction de création dun bouton, porte, et entité logique (qui seront des classe d'un sprite)
	- utilisation de bool et de fonction pour activer des élement avec diférent state lorsque activé ou idle
	- Systeme d'id automatique en fonction de la position `i`et `j` dans `counter[i][j]`
- Adaptation de `Player.sol` pour faire tomber lorsqu'il n'est pas sur une tile
	- adaptation condition pour chaque coté pour stop dans chaque direction
------------
###### Graphique
- Lien entre menu
	-utilisation de `Tilemap::LoadLevel()`et`Tilemap::DrawLevel()`
- Faire les texture de blocks
- Faire animation personnage
------------
###### Press F to pay respect